from flask import Flask, jsonify, request

app = Flask(__name__)

incomes = [{"description": "salary", "amount": 1000000}]


@app.route("/incomes")
def get_incomes():
    return jsonify(incomes), 200


@app.route("/incomes", methods=["POST"])
def add_income():
    incomes.append(request.get_json())
    return "", 201
