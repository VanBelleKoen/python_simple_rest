```bash
pip install pipenv
pipenv --three
pipenv install flask
```

```bash
./bootstrap.sh
```

```bash
curl http://localhost:5000/incomes
```

```bash
curl -X POST -H "Content-Type: application/json" -d '{
  "description": "lottery",
  "amount": 1000.0
}' http://localhost:5000/incomes
```
